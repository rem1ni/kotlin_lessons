import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/*Сделать программу, которая запускает в фоновом потоке метод печати «World» с задержкой в 1 сек.
В основном потоке запускаем печать «Hello,» с задержкой в 2 сек., блокируем поток */

suspend fun main(args: Array<String>) = coroutineScope {
    val job = launch {
        while (true) {
            delay(1000)
            println("World")
        }
    }
    repeat (5) {
        delay(2000)
        println("Hello")
    }
    job.cancel()
}