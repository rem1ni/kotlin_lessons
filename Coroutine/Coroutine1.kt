import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/*Написать две функции с задержкой, которые будут возвращать 2 числа.
В main написать блок кода, который будет суммировать вызов этих 2 функций,
сначала написать последовательный вызов функций, вывести сумму и время работы,
потом написать блок с асинхронным вызовом и сравнить время работы, обосновать время*/
/* Дополнить код, чтоб программа выводила след текст в консоль.
«I'm sleeping 0 ...I'm sleeping 1 ... I'm sleeping 2 ... main:
I'm tired of waiting! I'm running finally main: Now I can quit.» */
suspend fun getFirstNumber(): Int {
    delay(3000)
    return (1..9).random()
}

suspend fun getSecondNumber(): Int {
    delay(3000)
    return (1..9).random()
}

suspend fun main(args: Array<String>) = coroutineScope {
    var timeStart =  System.currentTimeMillis()
    println("Sum: "  + (getFirstNumber() + getSecondNumber()))
    println("Time: " + (System.currentTimeMillis() - timeStart))

    var count: Int = 0
    timeStart = System.currentTimeMillis()
    val firstNumber = async { getFirstNumber() }
    val secondNumber = async { getSecondNumber() }
    while (!firstNumber.isCompleted || !secondNumber.isCompleted ) {
        println("I'm sleeping $count ...")
        count++
        delay(1000)
    }
    println("main:  I'm tired of waiting! I'm running finally main: Now I can quit.")
    println("Sum with async: "  + (firstNumber.await() + secondNumber.await()))
    println("Time with async: " + (System.currentTimeMillis() - timeStart))

    /* После вызова первой функции мы не дожидаемся ее завершения, а запускаем следующую функцию
    * и уже после дожидаемся получения ответов, что и сокращает время */
}